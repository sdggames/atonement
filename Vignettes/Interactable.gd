extends Area2D

signal damaged
signal redeemed

var interacted : bool = false

func interactable():
	return !interacted

func cover():
	var audio = $CoverAudio
	if (audio):
		audio.play()
	$"AnimationPlayer".play("damage")
	emit_signal("damaged")
	interacted = true
	var covering = $Covering
	GameSettings.scoreLoss()
	remove_child(covering)
	return covering

func atone():
	var audio = $AtoneAudio
	if (audio):
		audio.play()
	$"AnimationPlayer".play("redeem")
	emit_signal("redeemed")
	GameSettings.bonusScore()
	interacted = true
