extends Node2D

export(Array, Resource) var vignettes

func _ready():
	var chosen = vignettes[randi() % vignettes.size()].instance()
	add_child(chosen)
	$House_Pink_Base.queue_free()
	
