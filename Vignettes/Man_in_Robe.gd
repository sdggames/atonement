extends Area2D

signal redeemed

export var isFirst = true

var interacted : bool = false

func interactable():
	return !interacted

func cover():
	if isFirst:
		$AnimationPlayer.play("Conversation")

func atone():
	if isFirst:
		GameSettings.scrollScale = 0
		$AnimationPlayer.play("Conversation")
		yield($AnimationPlayer, "animation_finished")
		interacted = true

		if GameSettings.scrollScale == 0:
			GameSettings.scrollScale = 0.5
			$Robe.queue_free()
			$Man/Player_Torso.queue_free()
			emit_signal("redeemed")
			$AnimationPlayer.play("Wear It Well")
			GameSettings.superBonusScore = true

func _process(_delta):
	if !isFirst && !interacted:
		var collision = get_overlapping_areas()
		if !collision.empty():
			interacted = true
			$AnimationPlayer.play("Well Done")
			GameSettings.timeScale = 0
			yield($AnimationPlayer, "animation_finished")
			var success = get_tree().change_scene("res://UI/GameOver.tscn")
			assert(success == OK)

func _on_item_damaged():
	queue_free()
