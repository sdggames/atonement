extends Node2D

signal objectLeftScene

export var speed: float = 50.0
export var width: float = 1024

func _process(delta: float) -> void:
	if GameSettings.scrollScale > 0 && GameSettings.timeScale > 0:
		position.x -= GameSettings.timeScale * GameSettings.scrollScale * speed * delta
		if position.x < -width:
			emit_signal("objectLeftScene")
			queue_free()
