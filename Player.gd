extends KinematicBody2D

signal newCovering
signal gameLost

export(Resource) var newgame_covering
export var sprintMaxTime: float = 2
export var sprintRechangeRate: float = 0.5
export var sprintTimeScale: float = 2
export(Vector2) var coveringPosition = Vector2(0, 60)

var sprintTime: float = sprintMaxTime

var covering
var isInConversation = false
var isRedeemed = false
var runSpeed = 1

var vel : Vector2 = Vector2()

func _ready():
	covering = get_node_or_null("Covering")
	if covering:
		covering.enable()
	$Anim.play("Run")

func _process(delta):
	if !isInConversation:
		if Input.is_action_pressed("sprint"):
			sprintTime -= delta
			if sprintTime > 0:
				GameSettings.scrollScale = sprintTimeScale
			else:
				sprintTime = 0
				if GameSettings.scrollScale == sprintTimeScale:
					$BreathPlayer.play()
				GameSettings.scrollScale = runSpeed
		elif sprintTime < sprintMaxTime:
			sprintTime += delta * sprintRechangeRate
			GameSettings.scrollScale = runSpeed

func _input(event):
	var collision = $Area2D.get_overlapping_areas()
	if !collision.empty() && (GameSettings.timeScale != 0):
		var interactable = collision[0]
		
		if interactable.name == "Man_in_Robe" && interactable.interactable():
			if event.is_action_pressed("cover"):
				interactable.cover()
			
			if event.is_action_pressed("interact"):
				isInConversation = true
				runSpeed = 0.5
				$Anim.play("Idle")
				interactable.atone()
				yield(interactable, "redeemed")
				$Poof.emit()
				$Anim.playback_speed = 1
				$Anim.play("Walk_Redeemed")
				isRedeemed = true
				isInConversation = false
		
		elif interactable.has_method("interactable") && interactable.interactable():
			if event.is_action_pressed("cover"):
				runSpeed = 1
				cover(interactable)
			
			if event.is_action_pressed("interact"):
				interact(interactable)


func cover(var interactable):
	if (covering):
		covering.queue_free()
	
	if isRedeemed:
		isRedeemed = false
		$Anim.play("Run")
	
	covering = interactable.cover()
	connectNewCovering()
	$Poof.emit();


func interact(var interactable):
	interactable.atone()
	$Poof.emit();

func connectNewCovering():
	covering.enable()
	add_child(covering)
	covering.position = coveringPosition
	covering.connect("expired", self, "_on_Covering_expired")
	emit_signal("newCovering", covering)

func _on_Covering_expired():
	if (!isInConversation):
		emit_signal("gameLost")
	$GameOver.play()
	$Anim.play("GameLost")
	$Anim.playback_speed = 1

func _on_new_game():
	$Anim.play("Run")
	covering = newgame_covering.instance()
	connectNewCovering()
