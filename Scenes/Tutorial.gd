extends Control

enum tutorialStates {
	RIP,
	START,
	EXTRA_HINT,
	COVER1,
	PRECARIOUS,
	WINDY,
	HALT,
	COVER2,
	AREA2,
	HALT2,
	EXTRA_HINT2,
	INTERACT_NICE,
	END
}

# Dear future me: this is not the way to make a tutorial. Maybe use a script thingy or
# something next time. You re-wrote this, but left the nonsense in since it's too hard
# to remember what's used and what isn't. Never, ever look at this file again :-)
# Also, don't mix text, scripts, timers, and animation. It was a terrible idea.

export(Resource) var covering_bench
export(Resource) var covering_house

export(String) var mainScene = "res://Scenes/Main.tscn"
export(String) var playerText_embarrass = "Oh, no, How embarrassing!\nI need to do something, fast!"
export(String) var playerText_extraHint = "Try pressing Cover\nor pressing D on the keyboard."
export(String) var playerText_precarious = "\nWell, this is precarious!"
export(String) var playerText_windy = "This isn't going to last long!\nI should try to grab something else."
export(String) var playerText_cover2 = "\nUh oh, I better keep moving!"
export(String) var playerText_atone = "I feel bad about that, maybe I can\ndo something nice for someone."
export(String) var playerText_extraHint2 = "Try pressing Atone\nor pressing A on the keyboard."
export(String) var playerText_nice = "\nWell, I feel better."
export(String) var playerText_end = "Unfortunately, I'm still in a\npredicament. Better keep moving!"

onready var playerText = $Player/PlayerText
onready var animator = $AnimationPlayer
onready var ingame_covering_bench = $Chunk/Bench/Covering
onready var ingame_house = $Chunk3/House
onready var ingame_car = $Chunk4/Car
onready var player_area = $Player/Area2D

var currentState: int = tutorialStates.RIP
var covering
var runSpeed = 1
var sprinting: bool = false
var sprintingPossible = false

func _ready():
	GameSettings.timeScale = 0
	changeState(tutorialStates.RIP)
	yield(animator, "animation_finished")

	changeState(tutorialStates.START)
	yield(animator, "animation_finished")
	yield(get_tree().create_timer(5), "timeout")

	if currentState == tutorialStates.START:
		changeState(tutorialStates.EXTRA_HINT)

func _physics_process(_delta):
	if currentState == tutorialStates.PRECARIOUS || currentState == tutorialStates.WINDY || currentState == tutorialStates.COVER2:
		if sprintingPossible && (sprinting || Input.is_action_pressed("sprint")):
			GameSettings.scrollScale = 1.5
			$Player/Anim.playback_speed = 1
		else:
			GameSettings.scrollScale = 0.5
			$Player/Anim.playback_speed = 0.5


func changeState(var nextState):
	if nextState == tutorialStates.RIP:
		$Player/Anim.play("Oh_no")
		$RipAudio.play()
		animator.play("Rip!")

	elif nextState == tutorialStates.START:
		animator.play("Player_Speak_Cover_Fadein")
		playerText.text = playerText_embarrass
		
	elif nextState == tutorialStates.EXTRA_HINT:
		playerText.percent_visible = 0
		playerText.text = playerText_extraHint
		animator.play("Player_Speak_Continue")
		
	elif nextState == tutorialStates.COVER1:
		ingame_covering_bench.queue_free()
		covering = covering_bench.instance()
		$Chunk/PaperAudio.play()
		$Player.add_child(covering)
		$Player/Anim.play("Run")
		$Player/Anim.playback_speed = 0.5
		animator.play("Player_Speak_End")
		GameSettings.timeScale = 1
		GameSettings.scrollScale = 0.5
	
	elif nextState == tutorialStates.PRECARIOUS:
		playerText.percent_visible = 0
		playerText.text = playerText_precarious
		animator.play("Player_Speak_WearBar_Fadein")
		covering.connect("wearUpdate", $UI/Overlay/DurabilityBar, "_on_Covering_wearUpdate")
		covering.wearSpeed = 4
		covering.enable()
		
	elif nextState == tutorialStates.WINDY:
		playerText.percent_visible = 0
		playerText.text = playerText_windy
		animator.play("Player_Speak_Continue")

	elif nextState == tutorialStates.HALT:
		pass
	
	elif nextState == tutorialStates.COVER2:
		covering.queue_free()
		$Chunk3/PauseTrigger.queue_free()
		covering = ingame_house.cover()
		$Player.add_child(covering)
		covering.z_index = 10
		covering.position = Vector2(0, 75)
		covering.wearSpeed = 7
		covering.enable()
		$Player/Poof.emit()
		$Player/Anim.play("Run")
		$Player/Anim.playback_speed = 0.5
		covering.connect("wearUpdate", $UI/Overlay/DurabilityBar, "_on_Covering_wearUpdate")
		GameSettings.timeScale = 1
		GameSettings.scrollScale = 0.5
		playerText.percent_visible = 0
		animator.playback_speed = 1
		playerText.percent_visible = 0
		playerText.text = playerText_atone
		animator.play("Player_Speak_Interact_Fadein")
		
	elif nextState == tutorialStates.AREA2 || nextState == tutorialStates.HALT2:
		pass
	
	elif nextState == tutorialStates.EXTRA_HINT2:
		playerText.percent_visible = 0
		playerText.text = playerText_extraHint2
		animator.play("Player_Speak_Continue")
		animator.playback_speed = 1
	
	elif nextState == tutorialStates.INTERACT_NICE:
		playerText.percent_visible = 0
		playerText.text = playerText_nice
		ingame_car.atone()
		$Player/Poof.emit()
		GameSettings.score = 1000
		animator.play("Player_Speak_Score_FadeIn")
		
	elif nextState == tutorialStates.END:
		playerText.percent_visible = 0
		playerText.text = playerText_end
		animator.playback_speed = 1
		animator.play("Player_Speak_Continue")
		yield(animator, "animation_finished")
		yield(get_tree().create_timer(1.5), "timeout")
		$Player/Anim.play("Run")
		animator.play("Fin.")
		
	else:
		assert("Tutorial state machine tried to access an unrecognized state!", nextState)
	currentState = nextState


func _input(event):
	if event.is_action_pressed("interact"):
		if currentState == tutorialStates.HALT2 || currentState == tutorialStates.EXTRA_HINT2:
			changeState(tutorialStates.INTERACT_NICE)
			yield(animator, "animation_finished")
			yield(get_tree().create_timer(1), "timeout")
			changeState(tutorialStates.END)
		
	if event.is_action_pressed("cover"):
		if currentState == tutorialStates.START || currentState == tutorialStates.EXTRA_HINT:
			changeState(tutorialStates.COVER1)
			yield(get_tree().create_timer(2), "timeout")
			changeState(tutorialStates.PRECARIOUS)
			yield(animator, "animation_finished")
			yield(get_tree().create_timer(1), "timeout")
			animator.play("Player_Speak_End")
			yield(animator, "animation_finished")
			animator.play("Jump_Fadein")
			sprintingPossible = true
			
		elif currentState == tutorialStates.HALT:
			changeState(tutorialStates.COVER2)
			


func _on_WindyTrigger_area_entered(area):
	if area == player_area:
		if currentState == tutorialStates.PRECARIOUS:
			changeState(tutorialStates.WINDY)
			yield(animator, "animation_finished")
			yield(get_tree().create_timer(1), "timeout")
			animator.play("Player_Speak_End")

func _on_House_area_entered(area):
	if area == player_area:
		if currentState == tutorialStates.WINDY:
			changeState(tutorialStates.HALT)

func _on_PauseTrigger_area_entered(area):
	if area == player_area:
		if currentState == tutorialStates.HALT || currentState == tutorialStates.INTERACT_NICE:
			GameSettings.timeScale = 0
			$Player/Anim.play("Idle")
		if currentState == tutorialStates.AREA2:
			GameSettings.timeScale = 0
			$Chunk4/PauseTrigger.queue_free()
			$Player/Anim.play("Idle")
			changeState(tutorialStates.HALT2)
			yield(get_tree().create_timer(5), "timeout")
		if currentState == tutorialStates.HALT2:
			changeState(tutorialStates.EXTRA_HINT2)


func _on_HeyTrigger_area_entered(area):
	if area == player_area:
		changeState(tutorialStates.HEY)
		yield(get_tree().create_timer(1), "timeout")
		GameSettings.timeScale = 1

func _on_ExitTrigger_area_entered(area):
	if area == player_area:
		animator.playback_speed = 1
		animator.play("Scene")
		yield(animator, "animation_finished")
		var success = get_tree().change_scene(mainScene)
		assert(success == OK)

func _on_Sprint_button_down():
	sprinting = true

func _on_Sprint_button_up():
	sprinting = false

func _on_Car_area_entered(area):
	if area == player_area:
		if currentState == tutorialStates.COVER2:
			changeState(tutorialStates.AREA2)
