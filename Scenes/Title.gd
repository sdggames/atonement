extends Control

func _on_MainMenu_button_pressed():
	$Fade.play("Fade")
	yield($Fade, "animation_finished")
	var mainScene = preload("res://Scenes/Tutorial.tscn")
	
	var success = get_tree().change_scene_to(mainScene)
	assert(success == OK)
