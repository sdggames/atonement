extends Control

export var url: String

func _on_Label_pressed():
	var _err = OS.shell_open(url)

func _on_PlayAgain_pressed():
	var success = get_tree().change_scene("res://Scenes/Title.tscn")
	assert(success == OK)
