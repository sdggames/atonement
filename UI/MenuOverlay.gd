extends Control

signal new_game

export(Color) var scoreColor
export(Color) var scoreColorGaining
export(Color) var scoreColorLosing

var sprintPressed = false
var lastScore: float = 0

func _ready():
	GameSettings.gameActive = true
	if !GameSettings.musicEnabled:
		$MusicToggle.pressed = true
	if !GameSettings.audioEnabled:
		$AudioToggle.pressed = true


func _on_Interact_pressed():
	send_action("interact")

func _on_Cover_pressed():
	send_action("cover")

func _on_Player_newCovering(var covering):
	covering.connect("wearUpdate", $Overlay/DurabilityBar, "_on_Covering_wearUpdate")

func _on_Retry_pressed():
	$Overlay/Retry.visible = false
	GameSettings.gameActive = true
	GameSettings.score = 0
	lastScore = 0
	emit_signal("new_game")

func _process(_delta):
	if sprintPressed:
		send_action("sprint")
		
	var score = $Score
	if score:
		if abs(GameSettings.score - lastScore) > 100:
			if GameSettings.score - lastScore > 0:
				$Score.set("custom_colors/font_color", scoreColorGaining)
				lastScore += 11
			else:
				$Score.set("custom_colors/font_color", scoreColorLosing)
				lastScore -= 7
		else:
			lastScore = GameSettings.score
			$Score.set("custom_colors/font_color", scoreColor)
		score.text = str(lastScore)

func send_action(action: String):
	# Send to the _input function.
	var event = InputEventAction.new()
	event.action = action
	event.pressed = true
	Input.parse_input_event(event)

	# Enable this to be caught by a physics update.
	Input.action_press(action)	
	yield(get_tree().create_timer(0.1), "timeout")
	Input.action_release(action)

func game_over():
	$Overlay/Retry.visible = true
	if (GameSettings.score > GameSettings.highScore):
		GameSettings.highScore = GameSettings.score
	$Overlay/Retry/Score.text = "Score: " + str(GameSettings.score) + "\nHigh Score:" + str(GameSettings.highScore)
	GameSettings.gameActive = false

func _on_Sprint_button_down():
	sprintPressed = true

func _on_Sprint_button_up():
	sprintPressed = false

func _on_MusicToggle_toggled(button_pressed):
	GameSettings.musicEnabled = !button_pressed

func _on_AudioToggle_toggled(button_pressed):
	GameSettings.audioEnabled = !button_pressed
