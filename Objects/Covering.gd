extends Sprite

export(float) var durability = 100
export(float) var wearSpeed = 15
export(Array, Resource) var textureArray: Array

signal expired
signal wearUpdate

var enabled = false

# Called when the node enters the scene tree for the first time.
func _ready():
	if textureArray.size() > 0:
		texture = textureArray[randi() % textureArray.size()]

func enable():
	enabled = true

func _physics_process(delta):
	if enabled:
		durability -= wearSpeed * GameSettings.timeScale * delta
		if durability <= 0:
			emit_signal("expired")
			queue_free()
		else:
			emit_signal("wearUpdate", durability)
