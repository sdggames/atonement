extends Control

signal gameOver

func _ready():
	set_timescale(1)

func _on_Player_gameLost():
	end_game()

func _on_UI_new_game():
	start_game()

func _on_Start_pressed():
	start_game()
		
func start_game():
	set_timescale(1)

func set_timescale(var time: float):
	GameSettings.timeScale = time
	
func end_game():
	emit_signal("gameOver")
	set_timescale(0)
