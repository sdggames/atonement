extends Sprite

export(Array, Resource) var gameSprites

export var minScale: float = 0.4
export var maxScale: float = 0.7

func _ready():
	texture = gameSprites[randi() % gameSprites.size()]
	var itemScale: float = rand_range(minScale, maxScale)
	if (randi() % 2 == 0):
		scale = Vector2(itemScale, itemScale)
	else:
		scale = Vector2(-itemScale, itemScale)
