extends Node

export var timeScale: float = 1
export var scrollScale: float = 1
export var score: int = 0
export var highScore: int = 0
export var gameActive: bool = false
export var coverCost: int = 700
export var interactValue: int = 900
export var superBonusScore: bool = false

export var audioEnabled: bool = true setget audioEnabled_set, audioEnabled_get
export var musicEnabled: bool = true setget musicEnabled_set, musicEnabled_get

var tickCount = 0
var musicManager

# Randomize here since I'm always init in any scene.
func _init():
	randomize()

func _physics_process(_delta):
	tickCount += 1
	if gameActive && (tickCount % 6 == 0):
		score += int(10 * timeScale * scrollScale)

func bonusScore():
	if superBonusScore:
		score += interactValue * 10
	else:
		score += interactValue

func scoreLoss():
	superBonusScore = false
	score -= coverCost
	if score < 0:
		score = 0

func audioEnabled_set(var value: bool):
	audioEnabled = value
	var master_sound = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_mute(master_sound, !audioEnabled)
	
func audioEnabled_get():
	return audioEnabled

func musicEnabled_set(var value: bool):
	if value:
		Music.play()
	else:
		Music.stop()
	musicEnabled = value
	
func musicEnabled_get():
	return musicEnabled
