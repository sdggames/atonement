extends Node2D

export var sceneSpeed:float = 100
export(Array, Resource) var sceneList:Array
export(Resource) var robeChunk

var chunkCount: int = 0
onready var chunk0 = $Chunk
onready var chunk1 = $Chunk2

func create_new_chunk(var resource: Resource):
	chunk0 = chunk1
	chunk1 = resource.instance()
	add_child(chunk1)
	chunk1.speed = sceneSpeed
	chunk1.position = chunk0.position + Vector2(chunk0.width - 8, 0)
	chunk1.connect("objectLeftScene", self, "_on_Chunk_objectLeftScene")

func _on_Chunk_objectLeftScene():
	chunkCount += 1
	if (chunkCount % 10) == 0:
		create_new_chunk(robeChunk)
	else:
		create_new_chunk(sceneList[randi() % sceneList.size()])
