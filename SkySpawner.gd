extends Node2D

export var sceneSpeed:Vector2 = Vector2(30, 70)
export var instanceHeight:Vector2 = Vector2(256, 512)
export(Array, Resource) var sceneList:Array

func _ready():
	randomize()

func create_new_chunk(var resource: Resource):
	var node = resource.instance()
	add_child(node)
	node.speed = rand_range(sceneSpeed.x, sceneSpeed.y)
	node.position = Vector2(get_viewport().size.x + (node.width / 2), rand_range(instanceHeight.x, instanceHeight.y))
	node.connect("objectLeftScene", self, "_on_objectLeftScene")

func _on_objectLeftScene():
	var scene = sceneList[randi() % sceneList.size()]
	create_new_chunk(scene)
